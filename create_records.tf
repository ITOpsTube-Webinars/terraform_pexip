resource "aws_route53_record" "px-turn-001" {
  zone_id = var.aws_route53_record_px_turn_001_zone_id
  name    = var.aws_route53_record_px_turn_001_name
  type    = var.aws_route53_record_px_turn_001_type
  ttl     = var.aws_route53_record_px_turn_001_ttl
  records = [aws_eip.px-turn-001-eip.public_ip]
}

resource "aws_route53_record" "px-turn-002" {
  zone_id = var.aws_route53_record_px_turn_002_zone_id
  name    = var.aws_route53_record_px_turn_002_name
  type    = var.aws_route53_record_px_turn_002_type
  ttl     = var.aws_route53_record_px_turn_002_ttl
  records = [aws_eip.px-turn-002-eip.public_ip]
}


resource "aws_route53_record" "turnalb" {
  zone_id = var.aws_route53_record_turnalb_zone_id
  name    = var.aws_route53_record_turnalb_name
  type    = var.aws_route53_record_turnalb_type
  ttl     = var.aws_route53_record_turnalb_ttl
  records = [module.px-conf-edge-alb.this_lb_dns_name]
}

