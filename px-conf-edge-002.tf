module "px-conf-edge-002" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = var.px_conf_edge_002_name
  instance_count         = var.px_conf_edge_002_instance_count
  ami                    = var.px_conf_edge_002_ami_id
  instance_type          = var.px_conf_edge_002_instance_type
  key_name               = var.px_conf_edge_002_key_name
  monitoring             = var.px_conf_edge_002_monitoring
  vpc_security_group_ids = [module.PEXIP_CONFERENCE_SG.this_security_group_id]
  subnet_id              = module.vpc.public_subnets[1] 
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Name        = "px-conf-edge-002"
  }
}

resource "aws_eip" "px-conf-edge-002-eip" {
  instance = module.px-conf-edge-002.id[0]
  vpc      = true
}
