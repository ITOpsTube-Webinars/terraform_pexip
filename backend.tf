terraform {
  backend "s3" {
    bucket = "terraform-infra-versiond"
    key    = "pexip/state"
    region = "us-east-1"
  }
}
