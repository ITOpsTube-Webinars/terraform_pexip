module "px-conf-burst-002" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = var.px_conf_burst_002_name
  instance_count         = var.px_conf_burst_002_instance_count
  ami                    = var.px_conf_burst_002_ami_id
  instance_type          = var.px_conf_burst_002_instance_type
  key_name               = var.px_conf_burst_002_key_name
  monitoring             = var.px_conf_burst_002_monitoring
  vpc_security_group_ids = [module.PEXIP_MANAGEMENT_SG.this_security_group_id]
  subnet_id              = module.vpc.private_subnets[1] 
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Name        = "px-conf-burst-002"
  }
}
