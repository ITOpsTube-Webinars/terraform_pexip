resource "aws_iam_user" "pexip" {
  name = "pexip"
  path = "/system/"

  tags = {
    tag-key = "pexip"
  }
}


resource "aws_iam_user_policy" "pexip-burst" {
  name = "pexip-burst"
  user = aws_iam_user.pexip.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "pexipdescribe",
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Sid": "pexipstartstop",
            "Effect": "Allow",
            "Action": [
                "ec2:StartInstances",
                "ec2:StopInstances"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "ec2:ResourceTag/pexip-cloud": "px-mgmt"
                }
            }
        }
    ]
}
EOF
}
