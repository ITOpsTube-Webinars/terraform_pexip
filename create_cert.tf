module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> v2.0"

  domain_name  = var.acm_domain_name
  zone_id      = var.acm_zone_id

  subject_alternative_names = [
    var.acm_subject_alternative_names
  ]

  tags = {
    Name = "test.lpdh-v3.com"
  }
}
