module "px-turn-001" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = var.px_turn_001_name
  instance_count         = var.px_turn_001_instance_count
  ami                    = var.px_turn_001_ami_id
  instance_type          = var.px_turn_001_instance_type
  key_name               = var.px_turn_001_key_name
  monitoring             = var.px_turn_001_monitoring
  vpc_security_group_ids = [module.PEXIP_TURN_SG.this_security_group_id]
  subnet_id              = module.vpc.public_subnets[0] 
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Name        = "px-turn-001"
  }
}

resource "aws_eip" "px-turn-001-eip" {
  instance = module.px-turn-001.id[0]
  vpc      = true
}
