module "px-turn-002" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = var.px_turn_002_name
  instance_count         = var.px_turn_002_instance_count
  ami                    = var.px_turn_002_ami_id
  instance_type          = var.px_turn_002_instance_type
  key_name               = var.px_turn_002_key_name
  monitoring             = var.px_turn_002_monitoring
  vpc_security_group_ids = [module.PEXIP_TURN_SG.this_security_group_id]
  subnet_id              = module.vpc.public_subnets[1] 
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Name        = "px-turn-002"
  }
}

resource "aws_eip" "px-turn-002-eip" {
  instance = module.px-turn-002.id[0]
  vpc      = true
}
