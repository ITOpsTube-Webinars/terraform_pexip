module "px-mgmt" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = var.px_mgmt_name
  instance_count         = var.px_mgmt_instance_count
  ami                    = var.px_mgmt_ami_id
  instance_type          = var.px_mgmt_instance_type
  key_name               = var.px_mgmt_key_name
  monitoring             = var.px_mgmt_monitoring
  vpc_security_group_ids = [module.PEXIP_MANAGEMENT_SG.this_security_group_id]
  subnet_id              = module.vpc.private_subnets[0] 
  tags = {
    Terraform   = "true"
    Environment = "dev"
    Name        = "px-mgmt"
  }
}
