module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.70.0"
  name = var.vpc_name
  cidr = var.vpc_cidr
  azs  = var.availability_zone_names
  private_subnets = var.private_subnets
  public_subnets  = var.public_subnets
  enable_nat_gateway = var.enable_nat_gateway
  single_nat_gateway = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support
  enable_dhcp_options = var.enable_dhcp_options
  dhcp_options_domain_name = var.dhcp_options_domain_name
  dhcp_options_domain_name_servers = var.dhcp_options_domain_name_servers
  tags = {
    Terraform = "true"
    Environment = "pexip"
    Name = "pexip"
  }
  # insert the 15 required variables here
}

module "PEXIP_ALB_SG" {
  source = "terraform-aws-modules/security-group/aws"

  name        = var.PEXIP_ALB_SG_name
  description = var.PEXIP_ALB_SG_description
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks      = var.PEXIP_ALB_SG_ingress_cidr_blocks
  ingress_rules            = var.PEXIP_ALB_SG_ingress_rules
  egress_rules             = var.PEXIP_ALB_SG_egress_rules
}

module "PEXIP_TURN_SG" {
  source = "terraform-aws-modules/security-group/aws"

  name        = var.PEXIP_TURN_SG_name
  description = var.PEXIP_TURN_SG_description
  vpc_id      = module.vpc.vpc_id
  egress_rules             = var.PEXIP_TURN_SG_egress_rules
  ingress_with_cidr_blocks = [
    {
      from_port   = var.PEXIP_TURN_SG_tcp_port
      to_port     = var.PEXIP_TURN_SG_tcp_port
      protocol    = var.PEXIP_TURN_SG_tcp_protocol
      description = var.PEXIP_TURN_SG_tcp_description
      cidr_blocks = var.vpc_cidr
    },
    {
      from_port   = var.PEXIP_TURN_SG_tcp_port
      to_port     = var.PEXIP_TURN_SG_tcp_port
      protocol    = var.PEXIP_TURN_SG_udp_protocol
      description = var.PEXIP_TURN_SG_udp_description
      cidr_blocks = var.vpc_cidr
    }
  ]
}

module "PEXIP_CONFERENCE_SG" {
  source = "terraform-aws-modules/security-group/aws"

  name        = var.PEXIP_CONFERENCE_SG_name
  description = var.PEXIP_CONFERENCE_SG_description
  vpc_id      = module.vpc.vpc_id
  egress_rules             = var.PEXIP_CONFERENCE_SG_egress_rules
  ingress_rules            = var.PEXIP_CONFERENCE_SG_ingress_rules
  ingress_with_cidr_blocks = [
    {
      from_port   = var.PEXIP_TURN_SG_tcp_port
      to_port     = var.PEXIP_TURN_SG_tcp_port
      protocol    = var.PEXIP_TURN_SG_tcp_protocol
      description = var.PEXIP_TURN_SG_tcp_description
      cidr_blocks = var.vpc_cidr
    },
    {
      from_port   = var.PEXIP_TURN_SG_tcp_port
      to_port     = var.PEXIP_TURN_SG_tcp_port
      protocol    = var.PEXIP_TURN_SG_udp_protocol
      description = var.PEXIP_TURN_SG_udp_description
      cidr_blocks = var.vpc_cidr
    }
  ]
}

module "PEXIP_MANAGEMENT_SG" {
  source = "terraform-aws-modules/security-group/aws"

  name        = var.PEXIP_MANAGEMENT_SG_name
  description = var.PEXIP_MANAGEMENT_SG_description
  vpc_id      = module.vpc.vpc_id
  egress_rules             = var.PEXIP_MANAGEMENT_SG_egress_rules
  ingress_rules            = var.PEXIP_MANAGEMENT_SG_ingress_rules
  ingress_with_cidr_blocks = [
    {
      from_port   = var.PEXIP_TURN_SG_tcp_port
      to_port     = var.PEXIP_TURN_SG_tcp_port
      protocol    = var.PEXIP_TURN_SG_tcp_protocol
      description = var.PEXIP_TURN_SG_tcp_description
      cidr_blocks = var.vpc_cidr
    },
    {
      from_port   = var.PEXIP_TURN_SG_tcp_port
      to_port     = var.PEXIP_TURN_SG_tcp_port
      protocol    = var.PEXIP_TURN_SG_udp_protocol
      description = var.PEXIP_TURN_SG_udp_description
      cidr_blocks = var.vpc_cidr
    }
  ]
}
