module "px-conf-edge-alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = var.px_conf_edge_alb_name

  load_balancer_type = var.load_balancer_type

  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  security_groups    = [module.PEXIP_CONFERENCE_SG.this_security_group_id]

  target_groups = [
    {
      name_prefix      = var.target_groups_name_prefix
      backend_protocol = var.target_groups_backend_protocol
      backend_port     = var.target_groups_backend_port
      target_type      = var.target_groups_target_type
    }
  ]

  https_listeners = [
    {
      port               = var.https_listeners_https_port
      protocol           = var.https_listeners_protocal
      certificate_arn    = var.https_listeners_certificate_arn
      target_group_index = var.https_listeners_target_group_index
    }
  ]

  tags = {
    Environment = "Dev",
    Name        = "px-conf-edge-alb"
  }
}
