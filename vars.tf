variable "vpc_name" {
  description = "VPC name"
  default     = "pexip"
}

variable "vpc_cidr" {
  description = "CIDR block"
  default     = "10.0.0.0/16"
}

variable "vpc_cidr" {
  description = "CIDR block"
  default     = "10.0.0.0/16"
}

variable "availability_zone_names" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "private_subnets" {
  type    = list(string)
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnets" {
  type    = list(string)
  default = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
}

variable "enable_nat_gateway" {
  default = true
}

variable "single_nat_gateway" {
  default = true
}

variable "one_nat_gateway_per_az" {
  default = false
}

variable "enable_dns_hostnames" {
  default = true
}

variable "enable_dns_support" {
  default = true
}

variable "enable_dhcp_options" {
  default = true
}

variable "dhcp_options_domain_name" {
  description = "dhcp options domain name"
  default     = "test.com"
}

variable "dhcp_options_domain_name_servers" {
  description = list(string)
  default     = ["127.0.0.1", "10.0.0.2"]
}

variable "PEXIP_ALB_SG_name" {
  description = "PEXIP ALB Name"
  default     = "PEXIP_ALB_SG"
}

variable "PEXIP_ALB_SG_description" {
  description = "PEXIP ALB Description"
  default     = "Security group PEXIP ALB"
}

variable "PEXIP_ALB_SG_ingress_cidr_blocks" {
  description = list(string)
  default     = ["0.0.0.0/0"]
}

variable "PEXIP_ALB_SG_ingress_rules" {
  description = list(string)
  default     = ["https-443-tcp"]
}

variable "PEXIP_ALB_SG_egress_rules" {
  description = list(string)
  default     = ["all-all"]
}

variable "PEXIP_TURN_SG_name" {
  description = "PEXIP TURN SG Name"
  default     = "PEXIP_TURN_SG"
}

variable "PEXIP_TURN_SG_description" {
  description = "PEXIP TURN SG Description"
  default     = "Security group TURN"
}

variable "PEXIP_TURN_SG_egress_rules" {
  description = list(string)
  default     = ["all-all"]
}

variable "PEXIP_TURN_SG_tcp_port" {
  default     = 3478
}

variable "PEXIP_TURN_SG_tcp_protocol" {
  description = "PEXIP TURN SG protocol"
  default     = "tcp"
}

variable "PEXIP_TURN_SG_tcp_description" {
  description = "PEXIP TURN SG tcp description"
  default     = "TURN TCP PORT"
}

variable "PEXIP_TURN_SG_udp_protocol" {
  description = "PEXIP TURN SG udp protocol"
  default     = "udp"
}

variable "PEXIP_TURN_SG_udp_description" {
  description = "PEXIP TURN SG udp description"
  default     = "TURN UDP PORT"
}

variable "PEXIP_CONFERENCE_SG_name" {
  description = "PEXIP CONFERENCE SG Name"
  default     = "PEXIP_CONFERENCE_SG"
}

variable "PEXIP_CONFERENCE_SG_description" {
  description = "PEXIP CONFERENCE SG Description"
  default     = "Security group CONFERENCE"
}

variable "PEXIP_CONFERENCE_SG_egress_rules" {
  description = list(string)
  default     = ["all-all"]
}

variable "PEXIP_CONFERENCE_SG_ingress_rules" {
  description = list(string)
  default     = ["https-443-tcp"]
}

variable "PEXIP_MANAGEMENT_SG_name" {
  description = "PEXIP MANAGEMENT SG Name"
  default     = "PEXIP_MANAGEMENT_SG"
}

variable "PEXIP_MANAGEMENT_SG_description" {
  description = "PEXIP MANAGEMENT SG Description"
  default     = "Security group MANAGEMENT"
}

variable "PEXIP_MANAGEMENT_SG_egress_rules" {
  description = list(string)
  default     = ["all-all"]
}

variable "PEXIP_MANAGEMENT_SG_ingress_rules" {
  description = list(string)
  default     = ["https-443-tcp"]
}

variable "px_turn_002_name" {
  description = "px turn 002 name"
  default     = "px-turn-002"
}

variable "px_turn_002_instance_count" {
  description = "px turn 002 instance count"
  default     = 1
}

variable "px_turn_002_ami_id" {
  description = "px turn 002 ami id"
  default     = "ami-033f09a57a2e102f5"
}

variable "px_turn_002_instance_type" {
  description = "px turn 002 instance type"
  default     = "t2.micro"
}

variable "px_turn_002_key_name" {
  description = "px turn 002 key name"
  default     = "emma-key"
}

variable "px_turn_002_monitoring" {
  default = true
}

variable "px_turn_001_name" {
  description = "px turn 001 name"
  default     = "px-turn-001"
}

variable "px_turn_001_instance_count" {
  description = "px turn 001 instance count"
  default     = 1
}

variable "px_turn_001_ami_id" {
  description = "px turn 001 ami id"
  default     = "ami-033f09a57a2e102f5"
}

variable "px_turn_001_instance_type" {
  description = "px turn 001 instance type"
  default     = "t2.micro"
}

variable "px_turn_001_key_name" {
  description = "px turn 001 key name"
  default     = "emma-key"
}

variable "px_turn_001_monitoring" {
  default = true
}

variable "px_mgmt_name" {
  description = "px-mgmt name"
  default     = "px-mgmt"
}

variable "px_mgmt_instance_count" {
  description = "px mgmt instance count"
  default     = 1
}

variable "px_mgmt_ami_id" {
  description = "px mgmt ami id"
  default     = "ami-0fbe20d41cce33d58"
}

variable "px_mgmt_instance_type" {
  description = "px mgmt instance type"
  default     = "t2.micro"
}

variable "px_mgmt_key_name" {
  description = "px mgmt key name"
  default     = "emma-key"
}

variable "px_mgmt_monitoring" {
  default = true
}

variable "px_conf_transcode_001_name" {
  description = "px conf transcode 001 name"
  default     = "px-conf-transcode-001"
}

variable "px_conf_transcode_001_instance_count" {
  description = "px conf transcode 001 instance count"
  default     = 1
}

variable "px_conf_transcode_001_ami_id" {
  description = "px conf transcode 001 ami id"
  default     = "ami-06aa64dbd979196e6"
}

variable "px_conf_transcode_001_instance_type" {
  description = "px conf transcode 001 instance type"
  default     = "t2.micro"
}

variable "px_conf_transcode_001_key_name" {
  description = "px conf transcode 001 key name"
  default     = "emma-key"
}

variable "px_conf_transcode_001_monitoring" {
  default = true
}

variable "px_conf_transcode_002_name" {
  description = "px conf transcode 002 name"
  default     = "px-conf-transcode-002"
}

variable "px_conf_transcode_002_instance_count" {
  description = "px conf transcode 002 instance count"
  default     = 1
}

variable "px_conf_transcode_002_ami_id" {
  description = "px conf transcode 002 ami id"
  default     = "ami-06aa64dbd979196e6"
}

variable "px_conf_transcode_002_instance_type" {
  description = "px conf transcode 002 instance type"
  default     = "t2.micro"
}

variable "px_conf_transcode_002_key_name" {
  description = "px conf transcode 002 key name"
  default     = "emma-key"
}

variable "px_conf_transcode_002_monitoring" {
  default = true
}

variable "px_conf_edge_002_name" {
  description = "px conf edge 002 name"
  default     = "px-conf-edge-002"
}

variable "px_conf_edge_002_instance_count" {
  description = "px conf edge 002 instance count"
  default     = 1
}

variable "px_conf_edge_002_ami_id" {
  description = "px conf edge 002 ami id"
  default     = "ami-06aa64dbd979196e6"
}

variable "px_conf_edge_002_instance_type" {
  description = "px conf edge 002 instance type"
  default     = "t2.micro"
}

variable "px_conf_edge_002_key_name" {
  description = "px conf edge 002 key name"
  default     = "emma-key"
}

variable "px_conf_edge_002_monitoring" {
  default = true
}

variable "px_conf_edge_001_name" {
  description = "px conf edge 001 name"
  default     = "px-conf-edge-001"
}

variable "px_conf_edge_001_instance_count" {
  description = "px conf edge 001 instance count"
  default     = 1
}

variable "px_conf_edge_001_ami_id" {
  description = "px conf edge 001 ami id"
  default     = "ami-06aa64dbd979196e6"
}

variable "px_conf_edge_001_instance_type" {
  description = "px conf edge 001 instance type"
  default     = "t2.micro"
}

variable "px_conf_edge_001_key_name" {
  description = "px conf edge 001 key name"
  default     = "emma-key"
}

variable "px_conf_edge_001_monitoring" {
  default = true
}

variable "px_conf_burst_001_name" {
  description = "px conf burst 001 name"
  default     = "px-conf-burst-001"
}

variable "px_conf_burst_001_instance_count" {
  description = "px conf burst 001 instance count"
  default     = 1
}

variable "px_conf_burst_001_ami_id" {
  description = "px conf burst 001 ami id"
  default     = "ami-06aa64dbd979196e6"
}

variable "px_conf_burst_001_instance_type" {
  description = "px conf burst 001 instance type"
  default     = "t2.micro"
}

variable "px_conf_burst_001_key_name" {
  description = "px conf burst 001 key name"
  default     = "emma-key"
}

variable "px_conf_burst_001_monitoring" {
  default = true
}

variable "px_conf_burst_002_name" {
  description = "px conf burst 002 name"
  default     = "px-conf-burst-002"
}

variable "px_conf_burst_002_instance_count" {
  description = "px conf burst 002 instance count"
  default     = 1
}

variable "px_conf_burst_002_ami_id" {
  description = "px conf burst 002 ami id"
  default     = "ami-06aa64dbd979196e6"
}

variable "px_conf_burst_002_instance_type" {
  description = "px conf burst 002 instance type"
  default     = "t2.micro"
}

variable "px_conf_burst_002_key_name" {
  description = "px conf burst 002 key name"
  default     = "emma-key"
}

variable "px_conf_burst_002_monitoring" {
  default = true
}

variable "aws_route53_record_px_turn_001_zone_id" {
  description = "aws route53 record px turn 001 zone id"
  default     = "Z06484651X8XLH0LTMREU"
}

variable "aws_route53_record_px_turn_001_name" {
  description = "aws route53 record px turn 001 name"
  default     = "px-turn-001"
}

variable "aws_route53_record_px_turn_001_type" {
  description = "aws route53 record px turn 001 type"
  default     = "A"
}

variable "aws_route53_record_px_turn_001_ttl" {
  description = "aws route53 record px turn 001 ttl"
  default     = "300"
}

variable "aws_route53_record_px_turn_002_zone_id" {
  description = "aws route53 record px turn 002 zone id"
  default     = "Z06484651X8XLH0LTMREU"
}

variable "aws_route53_record_px_turn_002_name" {
  description = "aws route53 record px turn 002 name"
  default     = "px-turn-002"
}

variable "aws_route53_record_px_turn_002_type" {
  description = "aws route53 record px turn 002 type"
  default     = "A"
}

variable "aws_route53_record_px_turn_002_ttl" {
  description = "aws route53 record px turn 002 ttl"
  default     = "300"
}

variable "aws_route53_record_turnalb_zone_id" {
  description = "aws route53 record turnalb zone id"
  default     = "Z06484651X8XLH0LTMREU"
}

variable "aws_route53_record_turnalb_name" {
  description = "aws route53 record turnalb name"
  default     = "px-conf-edge"
}

variable "aws_route53_record_turnalb_type" {
  description = "aws route53 record turnalb type"
  default     = "CNAME"
}

variable "aws_route53_record_turnalb_ttl" {
  description = "aws route53 record turnalb ttl"
  default     = "300"
}

variable "acm_domain_name" {
  description = "acm domain name"
  default     = "test.lpdh-v3.com"
}

variable "acm_zone_id" {
  description = "acm zone id"
  default     = "Z06484651X8XLH0LTMREU"
}

variable "acm_subject_alternative_names" {
  description = "acm subject alternative names"
  default     = "*.test.lpdh-v3.com"
}

variable "px_conf_edge_alb_name" {
  description = "px conf edge alb names"
  default     = "px-conf-edge-alb"
}

variable "load_balancer_type" {
  description = "load balancer type"
  default     = "application"
}

variable "target_groups_name_prefix" {
  description = "target groups name prefix"
  default     = "pref-"
}

variable "target_groups_backend_protocol" {
  description = "target groups backend protocol"
  default     = "HTTP"
}

variable "target_groups_backend_port" {
  description = "target groups backend port"
  default     = 8080
}

variable "target_groups_target_type" {
  description = "target groups target type"
  default     = "instance"
}

variable "https_listeners_https_port" {
  description = "https listener https port"
  default     = 443
}

variable "https_listeners_protocal" {
  description = "https listener protocal"
  default     = 443
}

variable "https_listeners_certificate_arn" {
  description = "https listener certificate_arn"
  default     = "arn:aws:iam::123456789012:server-certificate/test_cert-123456789012"
}

variable "https_listeners_target_group_index" {
  description = "https listener target_group_index"
  default     = 0
}